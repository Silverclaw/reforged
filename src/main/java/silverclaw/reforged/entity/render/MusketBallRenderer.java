package silverclaw.reforged.entity.render;

import net.minecraft.client.render.entity.EntityRenderDispatcher;
import net.minecraft.client.render.entity.ProjectileEntityRenderer;
import net.minecraft.util.Identifier;
import silverclaw.reforged.ReforgedMod;
import silverclaw.reforged.entity.MusketBallEntity;

public class MusketBallRenderer extends ProjectileEntityRenderer<MusketBallEntity>{

	public MusketBallRenderer(EntityRenderDispatcher entityRenderDispatcher) {
		super(entityRenderDispatcher);
		// TODO Auto-generated constructor stub
	}

	@Override
	public Identifier getTexture(MusketBallEntity entity) {
		return new Identifier(ReforgedMod.ID, "textures/musket_ball.png");
	}

}
