package silverclaw.reforged.item.common;

public interface MaterialTool extends Tool, Material {

	float getMiningSpeedMultiplier();
	
	@Override
	default float getMiningSpeed() {
		return getMaterial().getMiningSpeed() * getMiningSpeedMultiplier();
	}
}
