package silverclaw.reforged.item.common;

import net.minecraft.block.BlockState;
import net.minecraft.entity.LivingEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public interface Damagable {

	void applyHitDamage(ItemStack stack, LivingEntity cause);

	void applyMiningDamage(ItemStack stack, LivingEntity cause);

	default boolean postHit(ItemStack stack, LivingEntity target, LivingEntity attacker) {
		applyHitDamage(stack, attacker);
		return true;
	}

	default boolean postMine(ItemStack stack, World world, BlockState state, BlockPos pos, LivingEntity miner) {
		if (DamageHelper.isHardBlock(world, state, pos)) {
			applyMiningDamage(stack, miner);
		}
		return true;
	}

}
