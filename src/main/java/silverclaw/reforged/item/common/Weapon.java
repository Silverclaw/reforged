package silverclaw.reforged.item.common;

import net.minecraft.entity.LivingEntity;
import net.minecraft.item.*;
import net.minecraft.item.Item.Settings;

public interface Weapon extends Damaging, Damagable {

	@Override
	default void applyHitDamage(ItemStack stack, LivingEntity cause) {
		DamageHelper.applyItemDamage(1, stack, cause);
	}
	
	@Override
	default void applyMiningDamage(ItemStack stack, LivingEntity cause) {
		DamageHelper.applyItemDamage(2, stack, cause);
	}
	
	public static Settings combatSettings() {
		return new Item.Settings().group(ItemGroup.COMBAT);
	}
}
