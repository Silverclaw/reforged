package silverclaw.reforged.item.common;

import java.util.UUID;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;

import net.minecraft.entity.EquipmentSlot;
import net.minecraft.entity.attribute.EntityAttributeModifier;
import net.minecraft.entity.attribute.EntityAttributes;

public interface Damaging {

	public static final UUID ATTACK_DAMAGE_MODIFIER_UUID = UUID.fromString("CB3F55D3-645C-4F38-A497-9C13A33DB5CF");
	public static final UUID ATTACK_SPEED_MODIFIER_UUID = UUID.fromString("FA233E1C-4180-4865-B01B-BCCE9785ACA3");

	public static final String MODIFIER_NAME = "Weapon modifier";

	default Multimap<String, EntityAttributeModifier> getModifiers(EquipmentSlot slot) {
		Multimap<String, EntityAttributeModifier> modifiers = HashMultimap.create();
		if (slot == EquipmentSlot.MAINHAND) {
			modifiers.put(EntityAttributes.ATTACK_DAMAGE.getId(),
					new EntityAttributeModifier(ATTACK_DAMAGE_MODIFIER_UUID, MODIFIER_NAME, this.getAttackDamage(),
							EntityAttributeModifier.Operation.ADDITION));
			modifiers.put(EntityAttributes.ATTACK_SPEED.getId(), new EntityAttributeModifier(ATTACK_SPEED_MODIFIER_UUID,
					MODIFIER_NAME, getAttackSpeed(), EntityAttributeModifier.Operation.ADDITION));
		}
		return modifiers;
	}

	float getAttackDamage();

	float getAttackSpeed();
}
