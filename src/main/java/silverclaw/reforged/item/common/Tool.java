package silverclaw.reforged.item.common;

import net.minecraft.entity.LivingEntity;
import net.minecraft.item.ItemStack;

public interface Tool extends Damagable {

	@Override
	default void applyHitDamage(ItemStack stack, LivingEntity cause) {
		DamageHelper.applyItemDamage(2, stack, cause);
	}
	
	@Override
	default void applyMiningDamage(ItemStack stack, LivingEntity cause) {
		DamageHelper.applyItemDamage(1, stack, cause);
	}
	
	float getMiningSpeed();
}
