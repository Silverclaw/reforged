package silverclaw.reforged.item.common;

import net.minecraft.block.BlockState;
import net.minecraft.entity.EquipmentSlot;
import net.minecraft.entity.LivingEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public interface DamageHelper {

	static void applyItemDamage(int amount, ItemStack stack, LivingEntity cause) {
		stack.damage(amount, cause, entity -> {
			entity.sendEquipmentBreakStatus(EquipmentSlot.MAINHAND);
		});
	}

	static boolean isHardBlock(World world, BlockState state, BlockPos pos) {
		return state.getHardness(world, pos) != 0.0f;
	}
}
