package silverclaw.reforged.item.common;

import net.minecraft.item.ToolMaterial;

public interface Material {

	ToolMaterial getMaterial();
}
