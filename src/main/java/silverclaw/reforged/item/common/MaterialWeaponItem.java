package silverclaw.reforged.item.common;

import net.minecraft.item.ToolMaterial;

public abstract class MaterialWeaponItem extends WeaponItem implements Weapon, Material {

	protected final ToolMaterial material;

	public MaterialWeaponItem(ToolMaterial material, float attackDamage, float attackSpeed) {
		super(attackDamage, attackSpeed, Weapon.combatSettings().maxDamage(material.getDurability()));
		this.material = material;
	}

	@Override
	public ToolMaterial getMaterial() {
		return material;
	}
}
