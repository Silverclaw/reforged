package silverclaw.reforged.item.common;

import net.minecraft.block.BlockState;
import net.minecraft.entity.LivingEntity;
import net.minecraft.item.*;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public abstract class WeaponItem extends Item implements Weapon {

	protected final float attackDamage;
	protected final float attackSpeed;

	public WeaponItem(float attackDamage, float attackSpeed) {
		this(attackSpeed, attackDamage, Weapon.combatSettings());
	}

	public WeaponItem(float attackDamage, float attackSpeed, Item.Settings settings) {
		super(settings);
		this.attackDamage = attackDamage;
		this.attackSpeed = attackSpeed;
	}

	@Override
	public float getAttackDamage() {
		return attackDamage;
	}

	@Override
	public float getAttackSpeed() {
		return attackSpeed;
	}

	@Override
	public boolean postHit(ItemStack stack, LivingEntity target, LivingEntity attacker) {
		return Weapon.super.postHit(stack, target, attacker);
	}

	@Override
	public boolean postMine(ItemStack stack, World world, BlockState state, BlockPos pos, LivingEntity miner) {
		return Weapon.super.postMine(stack, world, state, pos, miner);
	}
}
