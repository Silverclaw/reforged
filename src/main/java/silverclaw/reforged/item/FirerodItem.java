package silverclaw.reforged.item;

import java.util.List;
import java.util.Random;

import net.minecraft.advancement.criterion.Criterions;
import net.minecraft.block.*;
import net.minecraft.client.item.TooltipContext;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.damage.DamageSource;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.*;
import net.minecraft.particle.ParticleTypes;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.sound.*;
import net.minecraft.text.Text;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.util.math.*;
import net.minecraft.world.IWorld;
import net.minecraft.world.World;
import silverclaw.reforged.ReforgedTooltips;
import silverclaw.reforged.item.common.*;

public class FirerodItem extends WeaponItem {

	private static final float IGNITE_CHANCE = 0.3f;

	public FirerodItem() {
		super(2f, -2.2f, Weapon.combatSettings().maxDamage(16));
	}
	
	@Override
	public boolean useOnEntity(ItemStack stack, PlayerEntity user, LivingEntity entity, Hand hand) {
		DamageHelper.applyItemDamage(4, stack, entity);
		entity.damage(DamageSource.player(user), getAttackDamage() / 2f);
		if (!entity.isFireImmune()) {
			int fireTicks = 10 + user.world.getRandom().nextInt(20);
			entity.setFireTicks(fireTicks);
		}
		return true;
	}

	@Override
	public boolean postHit(ItemStack stack, LivingEntity target, LivingEntity attacker) {
		World world = attacker.getEntityWorld();
		Vec3d pos = target.getPos();
		if (world.isClient()) {
			for (int i = 0; i < 25; i++) {
				spawnSmokeParticle(world, pos);
			}
		} else {
			SoundEvent soundEvent = SoundEvents.ENTITY_GENERIC_EXTINGUISH_FIRE;
			if (attacker instanceof PlayerEntity) {
				PlayerEntity player = (PlayerEntity) attacker;
				world.playSoundFromEntity(player, target, soundEvent, SoundCategory.PLAYERS, 1, 1);
			}
			if (target instanceof PlayerEntity) {
				PlayerEntity player = (PlayerEntity) target;
				world.playSoundFromEntity(player, target, soundEvent, SoundCategory.PLAYERS, 1, 1);
			}
		}
		return super.postHit(stack, target, attacker);
	}

	public static void spawnSmokeParticle(World world, Vec3d pos) {
		Random random = world.getRandom();
		world.addParticle(ParticleTypes.SMOKE,
				pos.getX() + random.nextDouble() / 2.0D * (random.nextBoolean() ? 1 : -1),
				pos.getY(),
				pos.getZ()+ random.nextDouble() / 2.0D * (random.nextBoolean() ? 1 : -1),
				0.0D, 0.005D, 0.0D);
	}

	@Override
	public ActionResult useOnBlock(ItemUsageContext context) {

		PlayerEntity player = context.getPlayer();
		ItemStack itemStack = context.getStack();
		IWorld world = context.getWorld();

		applyMiningDamage(itemStack, player);
		if(world.getRandom().nextFloat() > IGNITE_CHANCE) {
			return ActionResult.FAIL;
		}
		
		BlockPos blockPos = context.getBlockPos();
		BlockPos blockPosTarget = blockPos.offset(context.getSide());
		BlockState blockState = world.getBlockState(blockPosTarget);

		boolean canIgnite = canIgnite(blockState, world, blockPosTarget);
		if (canIgnite) {
			world.playSound(player, blockPosTarget, SoundEvents.BLOCK_FIRE_AMBIENT, SoundCategory.BLOCKS, 1.0F,
					RANDOM.nextFloat() * 0.4F + 0.8F);
			BlockState blockStateFire = ((FireBlock) Blocks.FIRE).getStateForPosition(world, blockPosTarget);
			world.setBlockState(blockPosTarget, blockStateFire, 11);
			if (player instanceof ServerPlayerEntity) {
				Criterions.PLACED_BLOCK.trigger((ServerPlayerEntity) player, blockPosTarget, itemStack);
			}
			return ActionResult.SUCCESS;
		} else {
			return ActionResult.FAIL;
		}
	}

	public static boolean canIgnite(BlockState block, IWorld world, BlockPos pos) {
		BlockState blockState = ((FireBlock) Blocks.FIRE).getStateForPosition(world, pos);
		boolean isNetherPortal = false;
		for (Direction direction : Direction.values()) {
			if (world.getBlockState(pos.offset(direction)).getBlock() == Blocks.OBSIDIAN
					&& ((NetherPortalBlock) Blocks.NETHER_PORTAL).createAreaHelper(world, pos) != null) {
				isNetherPortal = true;
			}
		}
		return block.isAir() && (blockState.canPlaceAt(world, pos) || isNetherPortal);
	}
	
	@Override
	public void appendTooltip(ItemStack stack, World world, List<Text> tooltips, TooltipContext context) {
		tooltips.add(new TranslatableText(ReforgedTooltips.IGNITE_CHANCE, IGNITE_CHANCE * 100f));
	}
}
