package silverclaw.reforged.item;

import net.minecraft.block.BlockState;
import net.minecraft.item.*;
import silverclaw.reforged.item.common.Weapon;

public class KnifeItem extends SwordItem {

	private static final float MINING_SPEED_MULTIPLIER = 1.25f;

	public KnifeItem(ToolMaterial material) {
		super(material, 2, -2.2f, Weapon.combatSettings());
	}
	
	@Override
	public float getMiningSpeed(ItemStack stack, BlockState state) {
		return super.getMiningSpeed(stack, state) * MINING_SPEED_MULTIPLIER;
	}
}
