package silverclaw.reforged.item;

import net.minecraft.block.BlockState;
import net.minecraft.entity.LivingEntity;
import net.minecraft.item.*;
import net.minecraft.sound.SoundCategory;
import net.minecraft.sound.SoundEvents;
import net.minecraft.world.World;
import silverclaw.reforged.item.common.DamageHelper;
import silverclaw.reforged.item.common.Weapon;

public class GreatAxeItem extends AxeItem {

	private static final float MINING_SPEED_MULTIPLIER = 0.75f;
	
	public GreatAxeItem(ToolMaterial material) {
		super(material, 6f, -3.2f, Weapon.combatSettings());
	}

	public float getMiningSpeed(ItemStack stack, BlockState state) {
		return super.getMiningSpeed(stack, state) * MINING_SPEED_MULTIPLIER;
	}

	@Override
	public boolean postHit(ItemStack stack, LivingEntity target, LivingEntity attacker) {
		ItemStack offhandStack = target.getOffHandStack();
		if(offhandStack != null) {
			Item offhandItem = offhandStack.getItem();
			if(offhandItem != null && offhandItem == Items.SHIELD) {
				System.out.println("damage shield");
				int damage = (int) attackDamage - 1;
				DamageHelper.applyItemDamage(damage, offhandStack, attacker);
				World world = attacker.getEntityWorld();
				if(!world.isClient()) {
					float volume = 0.1f * damage + world.getRandom().nextFloat() * 0.4f;
					world.playSoundFromEntity(null, target, SoundEvents.ITEM_SHIELD_BREAK, SoundCategory.HOSTILE, volume, 1f);
				}
			}
		}
		return super.postHit(stack, target, attacker);
	}
}
