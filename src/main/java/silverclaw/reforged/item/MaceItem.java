package silverclaw.reforged.item;

import static net.minecraft.entity.effect.StatusEffects.*;

import java.util.Arrays;
import java.util.List;

import net.minecraft.client.item.TooltipContext;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.effect.*;
import net.minecraft.item.*;
import net.minecraft.text.Text;
import net.minecraft.text.TranslatableText;
import net.minecraft.world.World;
import silverclaw.reforged.ReforgedTooltips;
import silverclaw.reforged.item.common.MaterialWeaponItem;

public class MaceItem extends MaterialWeaponItem {

	private static final List<StatusEffect> EFFECTS = Arrays.asList(BLINDNESS, WEAKNESS, SLOWNESS);
	private final float stunChance;
	private final int stunDuration;

	public MaceItem(ToolMaterial material) {
		super(material, 3f, -3.2f);
		this.stunChance = (material.getAttackDamage() + 2f) / 8f;
		this.stunDuration = (int) (material.getAttackDamage() + 4f);
	}

	@Override
	public float getAttackDamage() {
		return 3;
	}
	
	@Override
	public float getAttackSpeed() {
		return -3.2f;
	}
	
	public float getStunChance() {
		return stunChance;
	}

	@Override
	public boolean postHit(ItemStack stack, LivingEntity target, LivingEntity attacker) {
		super.postHit(stack, target, attacker);
		if (attacker.world.getRandom().nextFloat() < stunChance) {
			for (StatusEffect effect : EFFECTS) {
				target.addStatusEffect(new StatusEffectInstance(effect, stunDuration));
			}
		}
		return true;
	}
	
	@Override
	public void appendTooltip(ItemStack stack, World world, List<Text> tooltips, TooltipContext context) {
		tooltips.add(new TranslatableText(ReforgedTooltips.STUN_CHANCE, stunChance * 100f));
		tooltips.add(new TranslatableText(ReforgedTooltips.STUN_DURATION, stunDuration));
	}
}
