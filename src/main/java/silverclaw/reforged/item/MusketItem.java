package silverclaw.reforged.item;

import java.util.function.Predicate;

import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.projectile.ArrowEntity;
import net.minecraft.entity.projectile.ProjectileEntity;
import net.minecraft.item.*;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.sound.SoundCategory;
import net.minecraft.sound.SoundEvents;
import net.minecraft.util.*;
import net.minecraft.world.World;
import silverclaw.reforged.item.common.Weapon;
import silverclaw.reforged.item.common.WeaponItem;

public class MusketItem extends WeaponItem {

	public MusketItem() {
		super(2f, -3f, Weapon.combatSettings());
	}
	
	public Predicate<ItemStack> getProjectiles() {
		return (stack) -> {
			return stack.getItem() == Items.GUNPOWDER;
		};
	}

	@Override
	public int getEnchantability() {
		return 1;
	}

	public TypedActionResult<ItemStack> use(World world, PlayerEntity user, Hand hand) {

		ItemStack itemStack = user.getStackInHand(hand);
		boolean creative = user.abilities.creativeMode;
		if (creative || isLoaded(itemStack)) {
			shoot(world, user, hand, itemStack, null, 4.0f, creative, 1.0F);
			return TypedActionResult.consume(itemStack);
		} else {
			return TypedActionResult.fail(itemStack);
		}
	}

	private static void shoot(World world, LivingEntity shooter, Hand hand, ItemStack musket, ItemStack ammuniation,
			float soundPitch, boolean creative, float speed) {

		if (!world.isClient) {
			ProjectileEntity projectileEntity = createArrow(world, shooter, musket, ammuniation);
			world.spawnEntity(projectileEntity);
			world.playSound(null, shooter.getX(), shooter.getY(), shooter.getZ(), SoundEvents.ITEM_CROSSBOW_SHOOT,
					SoundCategory.PLAYERS, 1.0F, soundPitch);
		}
	}

	public static boolean isLoaded(ItemStack stack) {
		CompoundTag compoundTag = stack.getTag();
		return compoundTag != null && compoundTag.getBoolean("Loaded");
	}

	public static void setLoaded(ItemStack stack, boolean charged) {
		CompoundTag compoundTag = stack.getOrCreateTag();
		compoundTag.putBoolean("Loaded", charged);
	}

	private static ProjectileEntity createArrow(World world, LivingEntity entity, ItemStack crossbow, ItemStack arrow) {

		ProjectileEntity projectileEntity = new ArrowEntity(world, entity);

		projectileEntity.setSound(SoundEvents.ITEM_CROSSBOW_HIT);

		return projectileEntity;
	}

	public UseAction getUseAction(ItemStack stack) {
		return UseAction.CROSSBOW;
	}

	@Override
	public int getMaxUseTime(ItemStack stack) {
		return 40;
	}
}
