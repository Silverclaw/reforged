package silverclaw.reforged;

import java.util.Locale;

import net.minecraft.item.*;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;
import silverclaw.reforged.item.*;
import silverclaw.reforged.item.common.MaterialWeaponItem;
import silverclaw.reforged.item.common.WeaponItem;

public class ReforgedItems {

	public static final Item FIREROD = registerItem(new FirerodItem());;

	public static final Item GREATAXE_WOOD = registerWeapon(new GreatAxeItem(ToolMaterials.WOOD));
	public static final Item GREATAXE_STONE = registerWeapon(new GreatAxeItem(ToolMaterials.STONE));
	public static final Item GREATAXE_IRON = registerWeapon(new GreatAxeItem(ToolMaterials.IRON));
	public static final Item GREATAXE_GOLD = registerWeapon(new GreatAxeItem(ToolMaterials.GOLD));
	public static final Item GREATAXE_DIAMOND = registerWeapon(new GreatAxeItem(ToolMaterials.DIAMOND));
	
	public static final Item KNIFE_WOOD = registerWeapon(new KnifeItem(ToolMaterials.WOOD));
	public static final Item KNIFE_STONE = registerWeapon(new KnifeItem(ToolMaterials.STONE));
	public static final Item KNIFE_IRON = registerWeapon(new KnifeItem(ToolMaterials.IRON));
	public static final Item KNIFE_GOLD = registerWeapon(new KnifeItem(ToolMaterials.GOLD));
	public static final Item KNIFE_DIAMOND = registerWeapon(new KnifeItem(ToolMaterials.DIAMOND));

	public static final Item MACE_WOOD = registerItem(new MaceItem(ToolMaterials.WOOD));
	public static final Item MACE_STONE = registerItem(new MaceItem(ToolMaterials.STONE));
	public static final Item MACE_IRON = registerItem(new MaceItem(ToolMaterials.IRON));
	public static final Item MACE_GOLD = registerItem(new MaceItem(ToolMaterials.GOLD));
	public static final Item MACE_DIAMOND = registerItem(new MaceItem(ToolMaterials.DIAMOND));

	public static final Item MUSKET = registerItem(new MusketItem());

	public static Item registerWeapon(ToolItem item) {
		String baseName = getBaseName(item);
		String materialName = getStandardMaterialName(item.getMaterial());
		String name = baseName + "_" + materialName;
		return registerItem(item, name);
	}

	private static Item registerItem(Item item, String name) {
		return Registry.register(Registry.ITEM, new Identifier(ReforgedMod.ID, name), item);
	}

	private static Item registerItem(WeaponItem item) {
		String name = getBaseName(item);
		return Registry.register(Registry.ITEM, new Identifier(ReforgedMod.ID, name), item);
	}

	public static Item registerItem(MaterialWeaponItem item) {
		String baseName = getBaseName(item);
		String materialName = getStandardMaterialName(item.getMaterial());
		String name = baseName + "_" + materialName;
		return registerItem(item, name);
	}
	
	private static String getBaseName(Item item) {
		return item.getClass().getSimpleName().replaceAll("Item", "").toLowerCase(Locale.ENGLISH);
	}

	private static String getStandardMaterialName(ToolMaterial material) {
		for (ToolMaterials standardMetarial : ToolMaterials.values()) {
			if (material == standardMetarial)
				return standardMetarial.name().toLowerCase(Locale.ENGLISH);
		}
		throw new IllegalArgumentException("Unsupported material: " + material.getClass().getName());
	}

	static void initialize() {

	}
}
