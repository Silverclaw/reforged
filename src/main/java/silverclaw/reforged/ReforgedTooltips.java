package silverclaw.reforged;

public class ReforgedTooltips {

	public static final String IGNITE_CHANCE = itemTooltip("ignite_chance");

	public static final String STUN_CHANCE =  itemTooltip("stun_chance");
	public static final String STUN_DURATION = itemTooltip("stun_duration");

	static String itemTooltip(String name) {
		return "item." + ReforgedMod.ID + "." + name + ".tooltip";
	}
}
