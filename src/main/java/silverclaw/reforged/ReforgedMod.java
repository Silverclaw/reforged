package silverclaw.reforged;

import net.fabricmc.api.ModInitializer;
import net.fabricmc.fabric.api.entity.FabricEntityTypeBuilder;
import net.minecraft.entity.*;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;
import silverclaw.reforged.entity.MusketBallEntity;

public class ReforgedMod implements ModInitializer {

	public static final String ID = "reforged";

	public static final EntityType<MusketBallEntity> MUSKET_BALL = Registry.register(Registry.ENTITY_TYPE,
			new Identifier(ID, "musket_ball"),
			FabricEntityTypeBuilder.create(EntityCategory.MISC, MusketBallEntity::new).build());

	@Override
	public void onInitialize() {
		ReforgedItems.initialize();
	}
}
