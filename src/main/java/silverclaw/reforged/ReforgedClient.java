package silverclaw.reforged;

import net.fabricmc.api.ClientModInitializer;
import net.fabricmc.fabric.api.client.rendereregistry.v1.EntityRendererRegistry;
import silverclaw.reforged.entity.render.MusketBallRenderer;

public class ReforgedClient implements ClientModInitializer {

	@Override
	public void onInitializeClient() {
		EntityRendererRegistry.INSTANCE.register(ReforgedMod.MUSKET_BALL,
				(dispatcher, context) -> new MusketBallRenderer(dispatcher));
	}

}
